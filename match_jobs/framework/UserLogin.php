<?php

	/**
     * Import the framework's necessary itens
    */
	include BASE_PATH . '/User.php';
	include BASE_PATH . '/PasswordHash.php';

	/**
	 * UserLogin - Handle user data
	 *
	 * Handle use data, do login and logout, check permission and redirect page for logged user.
	 *
	 * @package KMSMVC
	 * @since 0.1
	*/
	class UserLogin extends User
	{
		/** Properties */
		private $user;
		private $config;
		private $cookie_user = "ACT_USR";

		/**
		 * Class constructor
		 *
		 * Set the database, controller, parameter and user data.
		 *
		 * @since 0.1
		 * @access public
		 * @param object $db PDO Conexion object
		*/
		public function __construct( DAO $db, Configuration $configuration_ )
		{
			// Set DB (DAO)
			$this->db = $db;

			// Set the configuration
			$this->config = $configuration_;
		}

		/**
		 * User logged or not
		 *
		 * True if logged.
		 *
		 * @public
		 * @access public
		 * @var bol
		*/
		public $logged_in;

		/**
		 * Status message for login form
		 *
		 * @public
		 * @access public
		 * @var string
		*/
		public $login_status;

		/**
		 * Verify login
		 *
		 * Check if the user is logged or not and
		 * Set the properties $logged_in and $login_status.
		*/
		public function check_userlogin() 
		{
			// Receive user information in case it exists
			$userdata = NULL;

			/*
			 * Verify if exists a session with userdata key
			 * The parameter most be an array instead an HTTP POST
			*/
			if ( isset( $_SESSION['userdata'] )
				 && ! empty( $_SESSION['userdata'] )
				 && is_array( $_SESSION['userdata'] ) 
				 && ! isset( $_POST['userdata'] )
				)
			{
				// Set user data
				$userdata = $_SESSION['userdata'];

				// Make sure that ins't a HTTP POST
				$userdata['post'] = false;
			}

			/*
			 * Verify if exists a session with userdata key
			 * The parameter most be an array instead an HTTP POST
			*/
			if ( isset( $_POST['userdata'] )
				 && ! empty( $_POST['userdata'] )
				 && is_array( $_POST['userdata'] ) 
				)
			{
				// Set user data
				$userdata = $_POST['userdata'];

				// Make sure that ins't a HTTP POST
				$userdata['post'] = true;
			}

			// Check if the user has a SESSION or an associated COOKIE
			if ( ( !isset( $userdata ) || !is_array( $userdata ) ) && !isset($_COOKIE[$this->cookie_user]) )
			{
				// User isn't logged
				$this->logged_in = false;

				// Clean-up the status
				$this->login_status = null;

				// Return the login status
				return false;
			}
			else
			{
				// User is logged
				$this->logged_in = true;

				// Clean-up the status
				$this->login_status = null;

				// Check if the user session contains the necessary data
				if ( isset($_SESSION["userdata"]) )
				{
					// Create a new User instance
            		$this->user = new User();

					// Set user object
					$this->user->USER_ID	= $_SESSION["userdata"]['ID_USUARIO'];
					$this->user->USER_TYPE	= $_SESSION["userdata"]['ID_TIPO_USUARIO'];
					$this->user->NAME 		= $_SESSION["userdata"]['PRIMEIRO_NOME'];
					$this->user->SURNAME	= $_SESSION["userdata"]['SOBRENOME'];
					$this->user->BIRTH		= $_SESSION["userdata"]['DATA_NASCIMENTO'];
					$this->user->GENDER		= $_SESSION["userdata"]['SEXO'];
					$this->user->EMAIL		= $_SESSION["userdata"]['EMAIL'];
					$this->user->CEP		= $_SESSION["userdata"]['CEP'];
					$this->user->CITY 		= $_SESSION["userdata"]['CIDADE'];
					$this->user->UF			= $_SESSION["userdata"]['UF'];
					$this->user->ID_CBO		= $_SESSION["userdata"]['ID_CBO'];
					$this->user->CBO		= $_SESSION["userdata"]['CBO'];
				}
				else
				{
					// The user session is empty
					$this->user = false;
				}

				// Return the login status
				return $this->user;
			}
		}

		/**
		 * Verify login
		 *
		 * Set the properties $logged_in and $login_status. Also set an array with user and password		 
		*/
		public function doLogin() 
		{
			// Receive user information in case it exists
			$userdata = NULL;
			$post = false;

			/*
			 * Verify if it's a POST request
			 * Login case (HTTP POST)
			*/
			if ( strcmp('POST', $_SERVER['REQUEST_METHOD']) == 0 )
			{
				// Set user data
				$userdata = $_POST;

				// Make sure that ins't a HTTP POST
				$userdata['post'] = true;
			}

			// Check if the informations was informed correctly
			if ( !isset( $userdata ) || !is_array( $userdata ) )
			{
				// Return to login page
				$this->logout(true);
				return;
			}

			// Set a variable with a post value
			$post = $userdata['post'];

			// Remove a chave post do array userdata
			unset( $userdata['post'] );

			// Extract variables from user data
			extract( $userdata );

			// Check if exists an user and password
			if ( !isset( $user ) || !isset( $user_password ) )
			{
				$this->logged_in = false;
				$this->login_status = null;

				// Unset any existent user session
				$this->logout();
				return;
			}

			// SQL query
			$sql = 'SELECT USR.`ID_USUARIO`, USR.`ID_TIPO_USUARIO`, USR.`PRIMEIRO_NOME`, USR.`SOBRENOME`, 
				USR.`DATA_NASCIMENTO`, USR.`SEXO`, USR.`EMAIL`, USR.`SENHA`, USR.`DATA_CADASTRO`, USR.`SESSION_ID`,
				USR.`CEP`, USR.`CIDADE`, UF.`NOME` AS UF, CBO.`ID_CBO`, CBO.`DESCRICAO` AS CBO
				FROM 
					`USUARIO` as USR 
				LEFT JOIN 
					`UF` AS UF ON UF.`ID_UF` = USR.`UF`
				LEFT JOIN 
					`CBO` AS CBO ON CBO.`ID_CBO` = USR.`CBO`
				WHERE
					USR.`EMAIL` = "' . $user . '" AND
					USR.`DATA_FECHA` IS NULL LIMIT 1';

			// Check if user exists in the database
			$query = $this->db->query( $sql );

			// Check the query
			if ( !$query )
			{
				$this->logged_in = false;
				$this->login_status = 'Não foi possível acessar o banco de dados.';

				// Unset any existent user session
				$this->logout();
				return;
			}

			// Get data from user database
			$fetch = $query->fetch(PDO::FETCH_ASSOC);

			// Get user ID
			$user_id = (int) $fetch['ID_USUARIO'];

			// Check if user ID exists
			if ( empty( $user_id ) )
			{
				$this->logged_in = false;
				$this->login_status = 'Usuário não existente.';

				// Unset any existent user session
				$this->logout();
				return;
			}

			// ******************************* HASH MODULE *******************************
			// Base-2 logarithm of the iteration count used for password stretching
			$hash_cost_log2 = 8;

			// Do we require the hashes to be portable to older systems (less secure)?
			$hash_portable = FALSE;

			$phpass = new PasswordHash($hash_cost_log2, $hash_portable);
			// ***************************************************************************

			// Compare the password with HASH from DB
			if ( $phpass->CheckPassword( $user_password, $fetch['SENHA'] ) )
			{
				// Compare the sessionID with DB session
				if ( session_id() != $fetch['SESSION_ID'] && ! $post )
				{
					$this->logged_in = false;
					$this->login_status = 'Wrong session ID.';

					// Unset any existent user session
					$this->logout();
					return;
				}

				// If it's a post session
				if ( $post )
				{
					// Generate new ID to session
					session_regenerate_id();
					$session_id = session_id();

					// Send the data to the session
					$_SESSION['userdata'] = $fetch;

					// Update password
					$_SESSION['userdata']['user_password'] = $user_password;

					// Update session ID
					$_SESSION['userdata']['user_session_id'] = $session_id;

					// Update session ID in database
					$query = $this->db->query( 'UPDATE `USUARIO` SET SESSION_ID = ? WHERE `ID_USUARIO` = ?', array( $session_id, $user_id ) );
				}

				// Get an array with user permission
				//$_SESSION['userdata']['user_permissions'] = unserialize( $fetch['user_permissions'] );
				$_SESSION['userdata']['user_permissions'] = "";

				// Set the property logged_in as logged
				$this->logged_in = true;

				// Password doesn't match
				$this->login_status = 'Usuário logado com sucesso.';

				// Set $this->userdata 
				$this->userdata = $_SESSION['userdata'];

				// **** CREATE COOKIE ****
				$id = $fetch['ID_USUARIO'];
				create_cookie( $phpass->HashPassword($id), $this->cookie_user);
				//create_cookie( $id, $this->cookie_user);

				// Create a new User instance
            	$this->user = new User();

				// Set user object
				$this->user->USER_ID	= $fetch['ID_USUARIO'];
				$this->user->USER_TYPE	= $fetch['ID_TIPO_USUARIO'];
				$this->user->NAME 		= $fetch['PRIMEIRO_NOME'];
				$this->user->SURNAME	= $fetch['SOBRENOME'];
				$this->user->BIRTH		= $fetch['DATA_NASCIMENTO'];
				$this->user->GENDER		= $fetch['SEXO'];
				$this->user->EMAIL		= $fetch['EMAIL'];
				$this->user->CEP		= $fetch['CEP'];
				$this->user->CITY 		= $fetch['CIDADE'];
				$this->user->UF			= $fetch['UF'];
				$this->user->ID_CBO		= $fetch['ID_CBO'];
				$this->user->CBO		= $fetch['CBO'];

				// Return the user object
				return $this->user;
			}
			else
			{
				// User isn't logged
				$this->logged_in = false;

				// Password doesn't match
				$this->login_status = 'Usuário ou senha incorreto.';

				// Return empty
				return;
			}
		}

		/**
		 * Logout
		 *
		 * Unset everything about user.
		 *
		 * @param bool $redirect If true, redirect to login page
		 * @final
		*/
		function logout( $redirect = false )
		{
			// The login page cannot redirect to itself
			if ( strpos($_SERVER['REQUEST_URI'], "ModuloLogin") === false )
			{
				// User isn't logged
				$this->logged_in = false;

				// Clean-up the status
				$this->login_status = null;

				// Remove all data from $_SESSION['userdata']
				$_SESSION['userdata'] = array();

				// Only to make sure (it isn't really needed)
				unset( $_SESSION['userdata'] );

				// Regenerates the session ID
				session_regenerate_id();

				// Kill the user cookie
				if ( isset($_COOKIE[$this->cookie_user]) )
				{
					unset($_COOKIE[$this->cookie_user]);
					setcookie($this->cookie_user, null, -1, '/');
				}

				if ( $redirect === true )
				{
					// Send the user to the login page
					$this->goto_login();
				}
			}
		}

		/**
		 * Go to login page
		*/
		protected function goto_login()
		{
			// Check if HOME URI is setted
			if ( isset($this->config->HOME_URI) )
			{
				// Set login URL
				$login_uri  = $this->config->HOME_URI . '/ModuloLogin/';

				// Set the current page
				$_SESSION['goto_url'] = urlencode( $_SERVER['REQUEST_URI'] );

				// Redirection
				echo '<meta http-equiv="Refresh" content="0; url=' . $login_uri . '">';
				echo '<script type="text/javascript">window.location.href = "' . $login_uri . '";</script>';
				// header('location: ' . $login_uri);
			}

			return;
		}

		/**
		 * Redirect to any page
		 *
		 * @final
		*/
		final protected function goto_page( $page_uri = null )
		{
			if ( isset( $_GET['url'] ) && ! empty( $_GET['url'] ) && ! $page_uri )
			{
				// Set URL
				$page_uri  = urldecode( $_GET['url'] );
			}

			if ( $page_uri )
			{ 
				// Redirect
				echo '<meta http-equiv="Refresh" content="0; url=' . $page_uri . '">';
				echo '<script type="text/javascript">window.location.href = "' . $page_uri . '";</script>';
				//header('location: ' . $page_uri);
				return;
			}
		}

		/**
		 * Check permission
		 *
		 * @param string $required The permission is required
		 * @param array $user_permissions User permissions
		 * @final
		*/
		final protected function check_permissions(
			$required = 'any', 
			$user_permissions = array('any')
		) {
			if ( ! is_array( $user_permissions ) ) {
				return;
			}

			// If user doesn't have the necessary permission
			if ( ! in_array( $required, $user_permissions ) ) {
				// Return false
				return false;
			} else {
				return true;
			}
		}
	}
?>