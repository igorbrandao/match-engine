<?php

interface Analysis 
{
    
    function get_accident_avg($Item_=null);
    
    function get_accident_median($Item_=null);
    
    function get_accident_mode($Item_=null);
    
}

?>