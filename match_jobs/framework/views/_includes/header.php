<!doctype html>
<!-- paulirish.com/2008/conditional-stylesheets-vs-css-hacks-answer-neither/ -->
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="no-js lt-ie9 lt-ie8" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="no-js lt-ie9" lang="en"> <![endif]-->

<!-- Consider adding a manifest.appcache: h5bp.com/d/Offline -->
<!--[if gt IE 8]><!--> <html lang="pt-br" class="no-js"> <!--<![endif]-->
	<head>

		<?php
			//**********************************************************************************
			//**********  DÉBORA, IGOR, NETO 2016 ©
			//**********
			//**********  PGM: ACT - Acidentes de Trabalho (FRAMEWORK)
			//**********
			//**********  CLIENTE:	PS - Projeto de Software
			//**********
			//**********  DÉBORA, IGOR, NETO
			//**********  VERSÃO:	1.0
			//**********
			//**********  OUT/2016 - Criação
			//**********  NOV/2016 - Atualização
			//**********
			//**********************************************************************************

			/* Verifica se a variável global existe e evita acesso direto a este arquivo */
			//if ( ! defined('ABSPATH')) exit;
		?>

		<!-- Title -->
	<title>
		<?php

			if ( isset($this->mvc_controller->title) )
				echo $this->mvc_controller->title;
			else
				echo $this->configuration->APP_NAME;
		?>
	</title>
	
		<!-- Meta tags -->
		<meta charset="utf-8">

		<link rel="dns-prefetch" href="https://fonts.googleapis.com" />
		<link rel="dns-prefetch" href="https://themes.googleusercontent.com" />
		<link rel="dns-prefetch" href="https://ajax.googleapis.com" />
		<link rel="dns-prefetch" href="https://cdnjs.cloudflare.com" />
		<link rel="dns-prefetch" href="https://agorbatchev.typepad.com" />

		<!-- Use the .htaccess and remove these lines to avoid edge case issues.
		More info: h5bp.com/b/378 -->
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

		<!-- Mobile viewport optimized: h5bp.com/viewport -->
		<meta name="viewport" content="width=device-width,initial-scale=1">
		<!-- iPhone: Don't render numbers as call links -->
		<meta name="format-detection" content="telephone=no">

		<!-- Keywords -->
		<meta name="keywords" content="ACT, acidente, trabalho, acidente de trabalho">

		<!-- Description -->
		<meta name="description" content="<?php echo $this->configuration->APP_NAME; ?>">

		<!-- Authors -->
		<meta name="author" content="Débora, Igor, Neto">

		<!-- Favicon -->
		<link rel="shortcut icon" href="<?php echo $this->configuration->HOME_URI;?>//framework/assets/img/logos/<?php echo $this->configuration->APP_NAME; ?>/favicon.png" />

		<!-- CSS -->

		<!-- Layout Styles -->
		<link rel="stylesheet" type="text/css" href="<?php echo $this->configuration->HOME_URI;?>/framework/assets/css/style.css?<?php echo time(); ?>">
		<link rel="stylesheet" type="text/css" href="<?php echo $this->configuration->HOME_URI;?>/framework/assets/css/grid.css?<?php echo time(); ?>">
		<link rel="stylesheet" type="text/css" href="<?php echo $this->configuration->HOME_URI;?>/framework/assets/css/layout.css?<?php echo time(); ?>">

		<!-- Icon Styles -->
		<link rel="stylesheet" type="text/css" href="<?php echo $this->configuration->HOME_URI;?>/framework/assets/css/icons.css?<?php echo time(); ?>">
		<link rel="stylesheet" type="text/css" href="<?php echo $this->configuration->HOME_URI;?>/framework/assets/css/fonts/font-awesome.css?<?php echo time(); ?>">
		<!--[if IE 8]><link rel="stylesheet" type="text/css" href="<?php echo $this->configuration->HOME_URI;?>/framework/assets/css/fonts/font-awesome-ie7.css?<?php echo time(); ?>"><![endif]-->

		<!-- External Styles -->
		<link rel="stylesheet" type="text/css" href="<?php echo $this->configuration->HOME_URI;?>/framework/assets/css/external/jquery-ui-1.8.21.custom.css?<?php echo time(); ?>">
		<link rel="stylesheet" type="text/css" href="<?php echo $this->configuration->HOME_URI;?>/framework/assets/css/external/jquery.chosen.css?<?php echo time(); ?>">
		<link rel="stylesheet" type="text/css" href="<?php echo $this->configuration->HOME_URI;?>/framework/assets/css/external/jquery.cleditor.css?<?php echo time(); ?>">
		<link rel="stylesheet" type="text/css" href="<?php echo $this->configuration->HOME_URI;?>/framework/assets/css/external/jquery.colorpicker.css?<?php echo time(); ?>">
		<link rel="stylesheet" type="text/css" href="<?php echo $this->configuration->HOME_URI;?>/framework/assets/css/external/jquery.elfinder.css?<?php echo time(); ?>">
		<link rel="stylesheet" type="text/css" href="<?php echo $this->configuration->HOME_URI;?>/framework/assets/css/external/jquery.fancybox.css?<?php echo time(); ?>">
		<link rel="stylesheet" type="text/css" href="<?php echo $this->configuration->HOME_URI;?>/framework/assets/css/external/jquery.jgrowl.css?<?php echo time(); ?>">
		<link rel="stylesheet" type="text/css" href="<?php echo $this->configuration->HOME_URI;?>/framework/assets/css/external/jquery.plupload.queue.css?<?php echo time(); ?>">
		<link rel="stylesheet" type="text/css" href="<?php echo $this->configuration->HOME_URI;?>/framework/assets/css/external/syntaxhighlighter/shCore.css?<?php echo time(); ?>" />
		<link rel="stylesheet" type="text/css" href="<?php echo $this->configuration->HOME_URI;?>/framework/assets/css/external/syntaxhighlighter/shThemeDefault.css?<?php echo time(); ?>" />

		<!-- Elements -->
		<link rel="stylesheet" type="text/css" href="<?php echo $this->configuration->HOME_URI;?>/framework/assets/css/elements.css?<?php echo time(); ?>">
		<link rel="stylesheet" type="text/css" href="<?php echo $this->configuration->HOME_URI;?>/framework/assets/css/forms.css?<?php echo time(); ?>">

		<!-- OPTIONAL: Print Stylesheet for Invoice -->
		<link rel="stylesheet" type="text/css" href="<?php echo $this->configuration->HOME_URI;?>/framework/assets/css/print-invoice.css?<?php echo time(); ?>">

		<!-- Typographics -->
		<link rel="stylesheet" type="text/css" href="<?php echo $this->configuration->HOME_URI;?>/framework/assets/css/typographics.css?<?php echo time(); ?>">

		<!-- Responsive Design -->
		<link rel="stylesheet" type="text/css" href="<?php echo $this->configuration->HOME_URI;?>/framework/assets/css/media-queries.css?<?php echo time(); ?>">

		<!-- Bad IE Styles -->
		<link rel="stylesheet" type="text/css" href="<?php echo $this->configuration->HOME_URI;?>/framework/assets/css/ie-fixes.css?<?php echo time(); ?>">

		<!-- JS -->
		<script src="<?php echo $this->configuration->HOME_URI;?>/framework/functions/global-functions.js"></script>

		<!-- JavaScript at the top (will be cached by browser) -->
			
		<!-- Load Webfont loader -->
		<script type="text/javascript">
			window.WebFontConfig = {
				google: { families: [ 'PT Sans:400,700' ] },
				active: function(){ $(window).trigger('fontsloaded') }
			};
		</script>
		<script defer async src="https://ajax.googleapis.com/ajax/libs/webfont/1.0.28/webfont.js"></script>
		
		<!-- Essential polyfills -->
		<script src="<?php echo $this->configuration->HOME_URI;?>/framework/assets/js/mylibs/polyfills/modernizr-2.6.1.min.js"></script>
		<script src="<?php echo $this->configuration->HOME_URI;?>/framework/assets/js/mylibs/polyfills/respond.js"></script>
		<script src="<?php echo $this->configuration->HOME_URI;?>/framework/assets/js/mylibs/polyfills/matchmedia.js"></script>
		<!--[if lt IE 9]><script src="<?php echo $this->configuration->HOME_URI;?>/framework/assets/js/mylibs/polyfills/selectivizr.js"></script><![endif]-->
		<!--[if lt IE 10]><script src="<?php echo $this->configuration->HOME_URI;?>/framework/assets/js/mylibs/polyfills/excanvas.js"></script><![endif]-->
		<!--[if lt IE 10]><script src="<?php echo $this->configuration->HOME_URI;?>/framework/assets/js/mylibs/polyfills/classlist.js"></script><![endif]-->

		<!-- Grab frameworks from CDNs -->
			<!-- Grab Google CDN's jQuery, with a protocol relative URL; fall back to local if offline -->
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.js"></script>
		<script>window.jQuery || document.write('<script src="<?php echo $this->configuration->HOME_URI;?>/framework/assets/js/libs/jquery-1.7.2.js"><\/script>')</script>
		
			<!-- Do the same with jQuery UI -->
		<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.21/jquery-ui.js"></script>
		<script>window.jQuery.ui || document.write('<script src="<?php echo $this->configuration->HOME_URI;?>/framework/assets/js/libs/jquery-ui-1.8.21.js"><\/script>')</script>
		
			<!-- Do the same with Lo-Dash.js -->
		<!--[if gt IE 8]><!-->
		<script src="<?php echo $this->configuration->HOME_URI;?>/framework/assets/js/libs/lo-dash.js"></script>
		<script>window._ || document.write('<script src="<?php echo $this->configuration->HOME_URI;?>/framework/assets/js/libs/lo-dash.js"><\/script>')</script>
		<!--<![endif]-->
		<!-- IE8 doesn't like lodash -->
		<!--[if lt IE 9]><script src="https://documentcloud.github.com/underscore/underscore.js"></script><![endif]-->

		<!-- start: MAIN JAVASCRIPTS -->
			<!-- General Scripts -->
			<script src="<?php echo $this->configuration->HOME_URI;?>/framework/assets/js/mylibs/jquery.hashchange.js"></script>
			<script src="<?php echo $this->configuration->HOME_URI;?>/framework/assets/js/mylibs/jquery.idle-timer.js"></script>
			<script src="<?php echo $this->configuration->HOME_URI;?>/framework/assets/js/mylibs/jquery.plusplus.js"></script>
			<script src="<?php echo $this->configuration->HOME_URI;?>/framework/assets/js/mylibs/jquery.jgrowl.js"></script>
			<script src="<?php echo $this->configuration->HOME_URI;?>/framework/assets/js/mylibs/jquery.scrollTo.js"></script>
			<script src="<?php echo $this->configuration->HOME_URI;?>/framework/assets/js/mylibs/jquery.ui.touch-punch.js"></script>
			<script src="<?php echo $this->configuration->HOME_URI;?>/framework/assets/js/mylibs/jquery.ui.multiaccordion.js"></script>
			<script src="<?php echo $this->configuration->HOME_URI;?>/framework/assets/js/mylibs/number-functions.js"></script>
			
			<!-- Forms -->
			<script src="<?php echo $this->configuration->HOME_URI;?>/framework/assets/js/mylibs/forms/jquery.autosize.js"></script>
			<script src="<?php echo $this->configuration->HOME_URI;?>/framework/assets/js/mylibs/forms/jquery.checkbox.js"></script>
			<script src="<?php echo $this->configuration->HOME_URI;?>/framework/assets/js/mylibs/forms/jquery.chosen.js"></script>
			<script src="<?php echo $this->configuration->HOME_URI;?>/framework/assets/js/mylibs/forms/jquery.cleditor.js"></script>
			<script src="<?php echo $this->configuration->HOME_URI;?>/framework/assets/js/mylibs/forms/jquery.colorpicker.js"></script>
			<script src="<?php echo $this->configuration->HOME_URI;?>/framework/assets/js/mylibs/forms/jquery.ellipsis.js"></script>
			<script src="<?php echo $this->configuration->HOME_URI;?>/framework/assets/js/mylibs/forms/jquery.fileinput.js"></script>
			<script src="<?php echo $this->configuration->HOME_URI;?>/framework/assets/js/mylibs/forms/jquery.fullcalendar.js"></script>
			<script src="<?php echo $this->configuration->HOME_URI;?>/framework/assets/js/mylibs/forms/jquery.maskedinput.js"></script>
			<script src="<?php echo $this->configuration->HOME_URI;?>/framework/assets/js/mylibs/forms/jquery.mousewheel.js"></script>
			<script src="<?php echo $this->configuration->HOME_URI;?>/framework/assets/js/mylibs/forms/jquery.placeholder.js"></script>
			<script src="<?php echo $this->configuration->HOME_URI;?>/framework/assets/js/mylibs/forms/jquery.pwdmeter.js"></script>
			<script src="<?php echo $this->configuration->HOME_URI;?>/framework/assets/js/mylibs/forms/jquery.ui.datetimepicker.js"></script>
			<script src="<?php echo $this->configuration->HOME_URI;?>/framework/assets/js/mylibs/forms/jquery.ui.spinner.js"></script>
			<script src="<?php echo $this->configuration->HOME_URI;?>/framework/assets/js/mylibs/forms/jquery.validate.js"></script>
			<script src="<?php echo $this->configuration->HOME_URI;?>/framework/assets/js/mylibs/forms/uploader/plupload.js"></script>
			<script src="<?php echo $this->configuration->HOME_URI;?>/framework/assets/js/mylibs/forms/uploader/plupload.html5.js"></script>
			<script src="<?php echo $this->configuration->HOME_URI;?>/framework/assets/js/mylibs/forms/uploader/plupload.html4.js"></script>
			<script src="<?php echo $this->configuration->HOME_URI;?>/framework/assets/js/mylibs/forms/uploader/plupload.flash.js"></script>
			<script src="<?php echo $this->configuration->HOME_URI;?>/framework/assets/js/mylibs/forms/uploader/jquery.plupload.queue/jquery.plupload.queue.js"></script>
				
			<!-- Charts -->
			<script src="<?php echo $this->configuration->HOME_URI;?>/framework/assets/js/mylibs/charts/jquery.flot.js"></script>
			<script src="<?php echo $this->configuration->HOME_URI;?>/framework/assets/js/mylibs/charts/jquery.flot.orderBars.js"></script>
			<script src="<?php echo $this->configuration->HOME_URI;?>/framework/assets/js/mylibs/charts/jquery.flot.pie.js"></script>
			<script src="<?php echo $this->configuration->HOME_URI;?>/framework/assets/js/mylibs/charts/jquery.flot.resize.js"></script>
			
			<!-- Explorer -->
			<script src="<?php echo $this->configuration->HOME_URI;?>/framework/assets/js/mylibs/explorer/jquery.elfinder.js"></script>
			
			<!-- Fullstats -->
			<script src="<?php echo $this->configuration->HOME_URI;?>/framework/assets/js/mylibs/fullstats/jquery.css-transform.js"></script>
			<script src="<?php echo $this->configuration->HOME_URI;?>/framework/assets/js/mylibs/fullstats/jquery.animate-css-rotate-scale.js"></script>
			<script src="<?php echo $this->configuration->HOME_URI;?>/framework/assets/js/mylibs/fullstats/jquery.sparkline.js"></script>
			
			<!-- Syntax Highlighter -->
			<script src="<?php echo $this->configuration->HOME_URI;?>/framework/assets/js/mylibs/syntaxhighlighter/shCore.js"></script>
			<script src="<?php echo $this->configuration->HOME_URI;?>/framework/assets/js/mylibs/syntaxhighlighter/shAutoloader.js"></script>
			
			<!-- Dynamic Tables -->
			<script src="<?php echo $this->configuration->HOME_URI;?>/framework/assets/js/mylibs/dynamic-tables/jquery.dataTables.js"></script>
			<script src="<?php echo $this->configuration->HOME_URI;?>/framework/assets/js/mylibs/dynamic-tables/jquery.dataTables.tableTools.zeroClipboard.js"></script>
			<script src="<?php echo $this->configuration->HOME_URI;?>/framework/assets/js/mylibs/dynamic-tables/jquery.dataTables.tableTools.js"></script>
			
			<!-- Gallery -->
			<script src="<?php echo $this->configuration->HOME_URI;?>/framework/assets/js/mylibs/gallery/jquery.fancybox.js"></script>
			
			<!-- Tooltips -->
			<script src="<?php echo $this->configuration->HOME_URI;?>/framework/assets/js/mylibs/tooltips/jquery.tipsy.js"></script>
			
			<!-- Do not touch! -->
			<script src="<?php echo $this->configuration->HOME_URI;?>/framework/assets/js/mango.js"></script>
			<script src="<?php echo $this->configuration->HOME_URI;?>/framework/assets/js/plugins.js"></script>
			<script src="<?php echo $this->configuration->HOME_URI;?>/framework/assets/js/script.js"></script>

			<!-- Your custom JS goes here -->

		<!-- end: MAIN JAVASCRIPTS -->

	</head>