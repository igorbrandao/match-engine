<!-- start: CONTENT -->
<section id="content" class="container_12 clearfix" data-sort=true>

	<?php

		// Insertion function
		$this->mvc_controller->getModel()->insert_user();

	?>

	<h1 class="grid_12 margin-top no-margin-top-phone" title="Cadastrar novo Usuário">Cadastrar novo Usuário</h1>

	<div class="grid_12">

		<form action="#" role="form" id="wiz" action="" method="POST" class="box wizard manual validate" enctype="multipart/form-data">

			<!--<div class="header">
				<h2><img src="<?php echo $this->configuration->HOME_URI;?>/framework/assets/img/icons/packs/fugue/16x16/ui-tab--arrow.png" class="icon">Wizard</h2>
			</div>-->

			<div class="content">

				<ul class="steps">
					<li><a class="current" href="#wiz_pessoal">Informações pessoais</a></li>
					<li><a href="#wiz_contato">Endereço e contato</a></li>
					<li><a href="#wiz_finalizacao">Finalização</a></li>
				</ul>

				<fieldset id="wiz_pessoal">

					<div class="row">
						<label for="ID_TIPO_USUARIO">
							<strong>Tipo de usuário: </strong>
							<small>(Preenchimento obrigatório)</small>
						</label>
						<div>
							<select name="ID_TIPO_USUARIO" id="ID_TIPO_USUARIO" class="required search">
								<option value="">Selecione...</option>
								<?php

									// User type list
									$list = $this->mvc_controller->getModel()->get_user_type_list($this->getUser()->USER_TYPE);

									foreach ( $list as $value )
									{
										echo "<option value='" . $value[0] . "'>" . $value[1] . "</option>";
									}

								?>
							</select>
						</div>
					</div>

					<div class="row">
						<label for="PRIMEIRO_NOME">
							<strong>Primeiro nome:</strong>
							<small>(Preenchimento obrigatório)</small>
						</label>
						<div>
							<input type="text" class="required" name="PRIMEIRO_NOME" id="PRIMEIRO_NOME" maxlength="30" />
						</div>
					</div>

					<div class="row">
						<label for="SOBRENOME">
							<strong>Sobrenome:</strong>
							<small>(Preenchimento obrigatório)</small>
						</label>
						<div>
							<input type="text" class="required" name="SOBRENOME" id="SOBRENOME" maxlength="30" />
						</div>
					</div>

					<div class="row">
						<label for="DATA_NASCIMENTO">
							<strong>Data de nascimento:</strong>
							<small>(Preenchimento obrigatório)</small>
						</label>
						<div>
							<input type="date" class="required" name="DATA_NASCIMENTO" id="DATA_NASCIMENTO" />
						</div>
					</div>

					<div class="row">
						<label for="SEXO">
							<strong>Sexo: </strong>
							<small>(Preenchimento obrigatório)</small>
						</label>
						<div>
							<select class="required search" name="SEXO" id="SEXO">
								<option value="">Selecione...</option> 
								<option value="M">Masculino</option> 
								<option value="F">Feminino</option> 
							</select>
						</div>
					</div>

				</fieldset>

				<fieldset id="wiz_contato">

					<div class="row">
						<label for="EMAIL">
							<strong>E-mail:</strong>
							<small>(Preenchimento obrigatório)</small>
						</label>
						<div>
							<input type="text" email="true" class="required" name="EMAIL" id="EMAIL" maxlength="100" />
						</div>
					</div>

					<div class="row">
						<label for="CEP">
							<strong>CEP:</strong>
							<small>(Preenchimento obrigatório)</small>
						</label>
						<div>
							<input type="text" class="required" name="CEP" id="CEP"/>
						</div>
					</div>

					<div class="row">
						<label for="CIDADE">
							<strong>Cidade:</strong>
							<small>(Preenchimento obrigatório)</small>
						</label>
						<div>
							<input type="text" class="required" name="CIDADE" id="CIDADE"/>
						</div>
					</div>

					<div class="row">
						<label for="UF">
							<strong>UF:</strong>
							<small>(Preenchimento obrigatório)</small>
						</label>
						<div>
							<select class="required search" name="UF" id="UF" class="search required">
								<option value="">Selecione...</option>
								<?php

									// Stete's list
									$list = $this->mvc_controller->getModel()->get_state_list();

									foreach ( $list as $value )
									{
										echo "<option value='" . $value[0] . "'>" . $value[1] . "</option>";
									}

								?>
							</select>
						</div>
					</div>

				</fieldset>

				<fieldset id="wiz_finalizacao">
					<div class="alert note top">
						<span class="icon"></span>
						<strong>Sucesso!</strong> Todas as informações foram corretamente preenchidas.
					</div>
					<p>Clique no botão &quotFinalizar cadastro&quot; para encerrar o cadastro. Uma mensagem de confirmação será enviado para o 
					e-mail cadastrado.</p>
				</fieldset>

			</div><!-- End of .content -->

			<div class="actions">
				<div class="left">
					<a href="#" class="button grey"><span><img src="<?php echo $this->configuration->HOME_URI;?>/framework/assets/img/icons/packs/fugue/16x16/arrow-180.png" width=16 height=16></span>Passo anterior</a>
				</div>
				<div class="right">
					<a href="#" class="button grey"><span><img src="<?php echo $this->configuration->HOME_URI;?>/framework/assets/img/icons/packs/fugue/16x16/arrow.png" width=16 height=16></span>Próximo passo</a>
					<a href="#" class="button finish"><span><img src="<?php echo $this->configuration->HOME_URI;?>/framework/assets/img/icons/packs/fugue/16x16/arrow.png" width=16 height=16></span>Finalizar cadastro</a>
				</div>
			</div><!-- End of .actions -->

		</form><!-- End of .box -->
	</div><!-- End of .grid_4 -->

	<script>
		$$.ready(function(){
			$('#wiz').wizard({
				onSubmit: function(){
					//alert('Your Data:\n' + $('form#wiz').serialize());
					return false;
				}
			});

			// Call the function to mask the input fields
			applyCustomMasks();
		});
	</script>
</section><!-- End of #content -->
<!-- end: CONTENT -->