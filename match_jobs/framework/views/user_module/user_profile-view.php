<!-- start: CONTENT -->
<section id="content" class="container_12 clearfix" data-sort=true>
	<div class="grid_12 profile">

		<?php

			// Check if service ID is valid
			if ( isset($_GET['USR']) && $_GET['USR'] != '' && strpos($_GET['USR'], "**") !== false )
			{
				$user_id = decrypted_url($_GET['USR'] , "**");

				// Load user information
				if ( is_numeric($user_id) )
				{
					$user_info = $this->mvc_controller->getModel()->get_user_info($user_id);
				}
			}

			// Check wich information will be used, session or $_GET
			if ( isset($user_id) )
			{
				// Information from $_GET
				$id 		= $user_info["ID_USUARIO"];
				$name		= $user_info["PRIMEIRO_NOME"] . " " . $user_info["SOBRENOME"];
				$user_type	= $user_info["ID_TIPO_USUARIO"];
				$years		= get_age($user_info["DATA_NASCIMENTO"]) . " anos";
				$born_date	= $user_info["DATA_NASCIMENTO"];
				$email		= $user_info["EMAIL"];
				$sex		= $user_info["SEXO"];

				$cep		= $user_info["CEP"];
				$city		= $user_info["CIDADE"];
				$uf			= $user_info["UF"];
				$cbo		= $user_info["CBO"];
			}
			else
			{
				// Information from session
				$id 		= $this->getUser()->USER_ID;
				$name		= $this->getUser()->NAME . " " . $this->getUser()->SURNAME;
				$user_type	= $this->getUser()->USER_TYPE;
				$years		= get_age( $this->getUser()->BIRTH ) . " anos";
				$born_date	= $this->getUser()->BIRTH;
				$email		= $this->getUser()->EMAIL;
				$sex		= $this->getUser()->GENDER;

				$cep		= $this->getUser()->CEP;
				$city		= $this->getUser()->CITY;
				$uf			= $this->getUser()->UF;
				$cbo		= $this->getUser()->CBO;
			}

		?>

		<div class="header">
		
			<div class="title">
				<h2><?php echo $name; ?></h2>
				<h3><?php 

						switch ( $user_type ) 
						{
							case 1:
								echo "Administrador";
								break;
							case 2:
								echo "Usuário";
								break;
							default:
								echo "Usuário";
								break;
						}

				?></h3>
			</div>
			<div class="avatar">
				<img style="height: 75px; width: 75px; border: 2; padding: 0 0 0 0px;" src="
					<?php 

						// Check if the user has a photo
						if ( isset($photo) && strcmp($photo, "") != 0 )
						{
							echo $this->configuration->HOME_URI . "/" . $photo;
						}
						else
						{
							echo $this->configuration->HOME_URI . "/framework/assets/img/logos/" . $this->configuration->APP_NAME . "/logo.png";
						}

					?>">
			</div>

			<ul class="info">

				<li>
					<a href="javascript:void(0);">
						<strong>IDADE</strong>
						<small><?php 

							if ( strcmp($born_date, "") != 0 )
							{
								echo $years;
							}
							else
							{
								echo "Não informada";
							}

						?></small>
					</a>
				</li>

			</ul><!-- End of ul.info -->
		</div><!-- End of .header -->

		<div class="details grid_12">
			<h2>Informações pessoais</h2>
			<?php 
				/**
				 * To edit the user info, it's necessary to be the self-user
				 * or the admin
				*/
				if ( isset($this->getUser()->USER_ID) && 
					( $this->getUser()->USER_ID == $id || $this->getUser()->USER_TYPE == ADMIN ) ) 
				{ 
			?>
				<a href="<?php echo encrypted_url($id, $this->configuration->HOME_URI . "/UserModule/user_edition?USR="); ?>"><span class="icon icon-pencil"></span>Editar informações do usuário</a>
			<?php } ?>
			<section>
				<table>
					<tr>
						<th>Nome:</th><td><?php 

						// Check if the user has a name
						if ( isset($name) && strcmp($name, "")  != 0 )
						{
							echo $name;
						}
						else
						{
							echo "Não informado";
						}

					?></td>
					</tr>
					<tr>
						<th>E-mail:</th><td><i><?php 

							// Check if the user has an email
							if ( isset($email) && strcmp($email, "")  != 0 )
							{
								echo $email;
							}
							else
							{
								echo "Não informado";
							}

						?></i></td>
					</tr>
					<tr>
						<th>Data de nascimento:</th><td><?php 

							// Check if the user has a birth date
							if ( isset($born_date) && strcmp($born_date, "")  != 0 )
							{
								echo $born_date;
							}
							else
							{
								echo "Não informada";
							}

						?></td>
					</tr>
					<tr>
						<th>Sexo:</th><td><?php 

							// Check if the user has a genre dedined
							if ( strcmp($sex, "") != 0 )
							{
								if ( strcmp($sex, "F") == 0 )
									echo "Feminino";
								else
									echo "Masculino";
							}
							else
							{
								echo "Não informado";
							}

						?></td>
					</tr>
				</table>
			</section>
		</div><!-- End of .details -->

		<div class="details grid_12">
			<h2>Ocupação</h2>
			<section>
				<table>
					<tr>
						<th>CBO:</th><td><?php

						if ( isset($cbo) && strcmp($cbo, "")  != 0 )
						{
							echo $cbo;
						}
						else
						{
							echo "Não informado";
						}

					?></td>
					</tr>
				</table>
			</section>
		</div><!-- End of .details -->

		<div class="details grid_12">
			<h2>Endereço</h2>
			<section>
				<table>
					<tr>
						<th>CEP:</th><td><?php

						if ( isset($cep) && strcmp($cep, "")  != 0 )
						{
							echo $cep;
						}
						else
						{
							echo "Não informado";
						}

					?></td>
					</tr>
					<tr>
						<th>Cidade:</th><td><?php 

							if ( isset($city) && strcmp($city, "")  != 0 )
							{
								echo $city;
							}
							else
							{
								echo "Não informado";
							}

						?></td>
					</tr>
					<tr>
						<th>Estado:</th><td><?php 

							if ( isset($uf) && strcmp($uf, "")  != 0 )
							{
								echo $uf;
							}
							else
							{
								echo "Não informado";
							}

						?></td>
					</tr>
				</table>
			</section>
		</div><!-- End of .details -->

		<div class="details grid_12">
			<h2>Localização</h2>
			<section>
				<table width="95%">
					<tr>
						<td>
							<!-- Height=450px; Width=600px -->
							<iframe src="https://www.google.com.br/maps?q=<?php echo $cep; ?>,%20Brasil&output=embed" 
							width="100%" height="450px" frameborder="0" style="border:0"></iframe>
						</td>
					</tr>
				</table>
			</section>
		</div><!-- End of .details -->
	</div>
</section><!-- End of #content -->
<!-- end: CONTENT -->