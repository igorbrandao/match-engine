<?php

    /**
	 * Page - define the page entity
	 *
	 * @since 0.1
	*/
    class Page
    {
        /**
         * Page attributes
        */
        public $views;

        /**
         * Page constructor
        */
        public function __construct()
        {
        	// Define the page structure (view)
            $this->views = array();

            // Add each view element
			$this->views['header']		= BASE_PATH . '/views/_includes/header.php';
			$this->views['loading_box'] = BASE_PATH . '/views/_includes/loading_box.php';
		    $this->views['lock_screen'] = BASE_PATH . '/views/_includes/lock_screen.php';
			//$this->views['dialogs']     = BASE_PATH . '/views/_includes/dialogs.php';       
			$this->views['message_box'] = BASE_PATH . '/views/_includes/message_box.php';
			$this->views['breadcrumb']	= ""; // Needs to be defined
			$this->views['navbar']		= BASE_PATH . '/views/_includes/navbar.php';
			$this->views['menu_logo']	= BASE_PATH . '/views/_includes/logo_menu.php';
			$this->views['toolbar'] 	= BASE_PATH . '/views/_includes/toolbar.php';
			$this->views['sidebar'] 	= BASE_PATH . '/views/_includes/sidebar.php';
			$this->views['view']		= ""; // Needs to be defined
			$this->views['footer']		= BASE_PATH . '/views/_includes/footer.php';
        }
    }

?>